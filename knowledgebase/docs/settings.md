# Settings

## Timezone

The Cloudron server is configured to be in UTC. This is intentional and should not be changed.

Cloudron has an internal timezone setting that controls various cron jobs like backup, updates,
date display in emails etc. This timezone can be changed from the settings view.

<center>
<img src="/img/settings-timezone.png" class="shadow" width="500px">
</center>

## Private Docker Registry

A private docker registry can be setup to pull the docker images of [custom apps](/custom-apps/tutorial/).

<center>
<img src="/img/settings-private-registry.png" class="shadow" width="500px">
</center>

!!! note "Use docker.io and not docker.com"
    If you are using the private DockerHub registry, use `docker.io` and not `docker.com`


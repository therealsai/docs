# Branding

The `Branding` view can be used to customize various aspects of the Cloudron like
it's name, logo and footer. The `Branding` view is only accessible by a Cloudron Owner.

## Cloudron Name

The Cloudron name in the `Branding` view. The name is used in the following places:

* Email templates - user invitations and notifications
* Dashboard header/navbar
* Login page

<center>
<img src="/img/branding-name.png" class="shadow" width="500px">
</center>

## Cloudron Avatar

The Cloudron avatar can be changed by clicking on the logo in the `Branding` view. The
avatar is used in the following places:

* Email templates - user invitations and notifications
* Dashboard header/navbar
* Login page

<center>
<img src="/img/branding-name.png" class="shadow" width="500px">
</center>

## Custom Pages

Custom pages can be setup under `/home/yellowtent/boxdata/custom_pages`.

| File  | Description  |
|---|---|
| `app_not_responding.html` | When any app becomes unresponsive |

## Footer

The footer can be branded with [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
from the `Branding` page.

<center>
<img src="/img/branding-footer.png" class="shadow" width="500px">
</center>


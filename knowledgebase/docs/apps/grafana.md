# <img src="/img/grafana-logo.png" width="25px"> Grafana App

## Customizations

Custom configuration can be added in the file `/app/data/custom.ini`. See the [Grafana docs](https://grafana.com/docs/grafana/latest/administration/configuration/)
on the various configuration options.

## Installing plugins

To install plugins, you run the grafana CLI tool using the [Web Terminal](/apps#web-terminal).

For example,

```
# /app/code/bin/grafana-cli -homepath /app/code -config /run/grafana/custom.ini plugins install grafana-worldmap-panel
```

## Reset admin password

```
# /app/code/bin/grafana-cli --homepath /app/code  --config /run/grafana/custom.ini admin reset-admin-password secret123 
```


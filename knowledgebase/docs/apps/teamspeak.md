# <img src="/img/teamspeak-logo.png" height="32px" style="vertical-align: middle;"> TeamSpeak App

## Initial Setup

After installation, check the app logs (in the `Console` section) to get the admin server token. You can now connect
using one of the [Teamspeak client](https://www.teamspeak.com/en/downloads/).

On first time connect, the client with ask for a privilege key. This is the same as the admin server token.

<img src="/img/teamspeak-privilege-key.png" class="shadow">

## License

To configure a Teamspeak license in an app instance, upload the license `.dat` file to `/app/data/licensekey.dat` using the web terminal or Cloudron cli tool, then restart the app from the dashboard.

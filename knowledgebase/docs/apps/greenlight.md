# <img src="/img/greenlight-logo.png" width="25px"> Greenlight App

## Creating a new user with a role

To create a new User, open a [Web terminal](https://docs.cloudron.io/apps/#web-terminal) and run the following commands:

```bash
# Information
# bundle exec rake user:create["name","email","password","role"]

# Creating an admin
bundle exec rake user:create["admin","admin@server.local","changeme","admin"]

# Creating a user - Can be usefull if there is no cloudron user
bundle exec rake user:create["user-noldap","user-noldap@server.local","changeme","user"]
```

For further documentation about user creation you can visit the official documentation. [Greenlight - Creating Accounts](https://docs.bigbluebutton.org/greenlight/gl-admin.html#creating-accounts)

## Customizing looks and content of Greenlight

Please visit the official [Greenlight Documentaion](https://docs.bigbluebutton.org/greenlight/gl-admin.html#site-settings) for information about the customization.

## Customizing the Landing Page

The [Official Documentation](https://docs.bigbluebutton.org/greenlight/gl-customize.html#customizing-the-landing-page).

You can follow the [Official Documentation](https://docs.bigbluebutton.org/greenlight/gl-customize.html#customizing-the-landing-page) with the Cloudron [File manager](https://docs.cloudron.io/apps/#file-manager).

Just note the path changes:

```
app/views/main/index.html.erb => /app/data/views/index.html.erv
config/locales/en.yml => /app/data/locales/en.yml
```
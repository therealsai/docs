# <img src="/img/etherpad-logo.png" width="25px"> Etherpad App

## Installing plugins

To install plugins or change the configuration, visit the admin
interface at `/admin`. Only Cloudron admins will have access to
this page.

A complete list of available plugins is available [here](https://static.etherpad.org/plugins.html).

## Admin user

The first user to login in will be an admin. Additional admins can be added by adding their username to the
`ep_cloudron.admins` array in the `/app/data/settings.json`

```
  "ep_cloudron": {
    "admins": [
      "username1",
      "username2"
    ]
  }
```

!!! warning ""
    The app has to be restarted after editing `/app/data/settings.json` and all users have to re-login to change their role

## Custom settings

Use a [Web terminal](/apps#web-terminal) and add any custom
settings to `/app/data/settings.json`.

!!! warning ""
    The app has to be restarted after editing `/app/data/settings.json`

### Make Documents Public

By default the app will always require login with a valid user.
To allow any visitor to create and edit documents, add the following to `/app/data/settings.json`:

```
  "requireAuthentication": false,
```

## Customizing CSS

The CSS and Javascript can be customized by editing the files at `/app/data/custom/`.
See the [etherpad docs](http://etherpad.org/doc/v1.2.7/#index_custom_static_files) for
more information.

### Dark mode

The app ships with the **colibris** theme/skin. This skin supports a dark mode through the [skinVariants](https://github.com/ether/etherpad-lite/blob/develop/settings.json.template#L93). To enable that, edit `/app/data/settings.json`:

```
  "skinVariants": "super-dark-toolbar super-dark-editor dark-background",
```

## API Access

The [Etherpad API](http://etherpad.org/doc/v1.3.0/#index_http_api) can be accessed by
obtaining the APIKEY. For this, open a [Web terminal](/apps#web-terminal)
and view the contents of the file `/app/data/APIKEY.txt`.

Example usage:

    curl https://etherpad.domain/api/1.2.7/listAllPads?apikey=c5513793f24a6fbba161e4497b26c734ff5b2701fad0f1211097ccb405ea65c7


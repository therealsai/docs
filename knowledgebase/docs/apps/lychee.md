# <img src="/img/lychee-logo.png" width="25px"> Lychee App

## User management

While lychee has a UI to change the password, there is no UI to reset the password.
If you forget the password, reset the password of the admin user, use the [Web terminal](/apps#web-terminal).

```
# sudo -E -u www-data php artisan lychee:reset_admin
```

After running the above command, just logout from Lychee and it will ask for creating the admin user again.
Note that existing data is not lost when resetting admin credentials.

## Using an existing folder structure

Lychee has it's own folder-structure and database. You have to re-upload or re-import all your photos to use them.


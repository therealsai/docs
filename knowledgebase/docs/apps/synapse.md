# <img src="/img/synapse-logo.png" width="25px"> Synapse App

## Post installation

### Step 1: Select Matrix IDs

Just like email, users on matrix has a unique universal id. These ids are
of the form `@username:domain.com`.

To give users a "memorable id", this app (also known as home server) is pre-setup to use the second level
domain for the domain part of the id (also known as the `server_name`). For example,
if you installed the app at `matrix-homeserver.example.com`, this app package will set the
`server_name` to `example.com`. This will generate user ids of the form `@username:example.com`.

If you require a different server name, use a [Web Terminal](/apps#web-terminal)
to edit `/app/data/configs/homeserver.yaml` and restart the app.

### Step 2: Delegation

Matrix clients and other matrix servers discover the Matrix server for an ID using
[Well-Known](https://www.iana.org/assignments/well-known-uris/well-known-uris.xhtml) URIs.
The Well-Known URI is a document that is served up from the `server_name` domain (i.e `example.com`)
that [delegates](https://github.com/matrix-org/synapse/blob/master/docs/delegate.md) the handling
to another server (i.e `matrix-homeserver.example.com`).

#### Step 2.1: Setup 

If `server_name` is hosted on Cloudron, all you have to do is to SSH into Cloudron and create the following file:

```
mkdir -p /home/yellowtent/boxdata/well-known/example.com/matrix
echo '{ "m.server": "matrix-homeserver.example.com:443" }' > /home/yellowtent/boxdata/well-known/example.com/matrix/server
```

Be sure to replace `example.com` with your `server_name` and `matrix-homeserver.example.com` with the location
of this app.

#### Step 2.2: Re-configure 

Go to the Location page of `example.com` and click Save. This essentialy re-generates the nginx config. The new
nginx config serves up .well-known docs from the above server location


#### Step 2.3 Verify setup

To verify that the above changes worked, try this command from your laptop/PC:

```
$ curl https://example.com/.well-known/matrix/server
{ "m.server": "matrix-homeserver.example.com:443" }
```

If `server_name` is not hosted on Cloudron, you have to arrange for the above file to be served based on
your setup.

!!! note "`.well-known` setup is required"
    The `.well-known` setup is required for matrix clients to connect. This is the case even when `server_name` matches the home
    server location. This is because this app sets up federation on port 443 but the default is port 8448.

### Step 3. Federation

[Federation setup](https://github.com/matrix-org/synapse/blob/master/docs/federate.md) is automatic.
Use the [Federation Tester](https://federationtester.matrix.org/) to verify that everything is setup properly.
Note you must enter the `server_name` (like `example.com`) in the form field in the website and NOT the location
of your home server (despite what the form says).

## Admin

To make an existing user an admin, open a [Web terminal](/apps#web-terminal) and
run the following command:

```
# PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE users SET admin=1 WHERE name='@user:example.com'"
UPDATE 1
```

## Customizations

Synapse offers a variety of [customizations](https://github.com/matrix-org/synapse/blob/develop/docs/sample_config.yaml).
To make changes, use a [Web Terminal](/apps#web-terminal)
to edit `/app/data/configs/homeserver.yaml` and restart the app.

## Home page

The `index.html` can be customized by editing `/app/data/index.html`. Note that any assets have to be embedded inline.


# <img src="/img/navidrome-logo.png" width="25px"> Navidrome App

## Custom Configuration

Custom configuration can be placed in `/app/data/config.toml`.
See the [Navidrome docs](https://www.navidrome.org/docs/usage/configuration-options/) for
all the options.

## CLI

To trigger a scan:

```
gosu cloudron:cloudron /app/code/navidrome -c /app/data/config.toml scan -f
```


# <img src="/img/limesurvey-logo.png" width="25px"> LimeSurvey App

## Command Line Tool

The CLI tool can run using the [Web Terminal](/apps#web-terminal) as follows:

```
sudo -E -u www-data php /app/code/application/commands/console.php
```

## Templates

Questionnaire templates can be downloaded from the LimeSurvey site [here](https://www.limesurvey.org/downloads/category/19-templates).


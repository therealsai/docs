# <img src="/img/guacamole-logo.png" width="25px"> Guacamole App

## RDP

Most Windows/RDP servers do not have a valid certificate installed. For this reason,
be sure to check the ignore server certificate checkbox in the Parameters section.

<center>
<img src="/img/guacamole-rdp.png" class="shadow">
</center>

## Guacamole menu

The Guacamole menu is a sidebar which is hidden until explicitly shown. On a desktop or other
device which has a hardware keyboard, you can show this menu by pressing **Ctrl+Alt+Shift**.
If you are using a mobile or touchscreen device that lacks a keyboard, you can also show the
menu by swiping right from the left edge of the screen.

See [the docs](https://guacamole.apache.org/doc/gug/using-guacamole.html#guacamole-menu) for
more information.


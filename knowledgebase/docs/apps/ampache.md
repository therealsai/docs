# <img src="/img/ampache-logo.png" width="25px"> Ampache App

## Customization

Use the [Web terminal](/apps#web-terminal) or the [File Manager](/apps#file-manager)
to edit custom configuration under `/app/data/config/ampache.cfg.php`. Full documentation
is available [here](https://github.com/ampache/ampache/wiki/Basic).

## CLI

[Ampache CLI](https://github.com/ampache/ampache/wiki/CLI) can be used to run various
common management tasks. You can use the CLI using the [Web terminal](/apps#web-terminal) 
as follows:

```
    sudo -u www-data /usr/bin/php /app/code/bin/install/catalog_update.inc
```

## Features

Ampache has a lot of exciting features that are worth exploring further:

* [Subscribe to Podcasts](https://github.com/ampache/ampache/wiki/Podcasts)
* [Icecast compatible Channels](https://github.com/ampache/ampache/wiki/Channels)
* [Democratic play](https://github.com/ampache/ampache/wiki/Democratic)
* [Subsonic API](https://github.com/ampache/ampache/wiki/subsonic)


# <img src="/img/privatebin-logo.png" width="25px"> PrivateBin App

## Customizations
 
Various PrivateBin settings can be configured by editing `/app/data/conf.php` using
the [Web terminal](/apps#web-terminal).

See the `conf.php` for various customizations.

## Custom template

You can set a [custom template](https://github.com/PrivateBin/PrivateBin/wiki/Templates)
as follows:

* Create the template in `/app/data/custom_template/custom.php`. The name `custom.php` is
  hardcoded in the package.

* Change the template name in `/app/data/conf/conf.php` to be `custom`.

* You can save additional js/css/img in `/app/data/custom_template/` and access them from the
  php script as `js/custom/..`, `css/custom/...`, `img/custom/...`.


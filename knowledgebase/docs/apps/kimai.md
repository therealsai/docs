# <img src="/img/kimai-logo.png" width="25px"> Kimai App

## Plugins

Kimai2 supports both free and paid plugins or sometimes called bundles.
The [marketplace](https://www.kimai.org/store/) offers a selection of mostly paid plugins, while there are many free ones on github. Plugins can be obtained either through git checkout or downloading and extracting a zip bundle into `/app/data/plugins`.

The following example installes the demo plugin using the [Web terminal](/apps#web-terminal) into the running app instance:

```
cd /app/data/plugins
git clone https://github.com/Keleo/DemoBundle.git
chown -R www-data.www-data .
cd /app/code
sudo -u www-data bin/console kimai:reload
```

## Customization

Use the [Web terminal](/apps#web-terminal)
to edit custom configuration under `/app/data/local.yaml`.

See [Kimai customization docs](https://www.kimai.org/configurations.html)
for more information.

Once the `local.yaml` is updated, restart the app from the Cloudron dashboard.

## Troubleshooting

The `local.yaml` format is [YAML](https://lzone.de//cheat-sheet/YAML) and thus the format has to be valid,
otherwise the app would not start up. Especially check the indentation!

If after an update the Kimai instance is not starting, check the logs and revisit the values in the `/app/data/local.yaml` if they are still supported in the new version.

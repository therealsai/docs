# <img src="/img/kutt-logo.png" width="25px"> Kutt App

## Custom config

Custom configuration can be put in `/app/data/env` using the [Web Terminal](/apps#web-terminal) or the
[File Manager](/apps/#file-manager).

## Registration

Registration is enabled by default. This can be disabled by settings `DISALLOW_REGISTRATION=true`
in `/app/data/env'

## 3rd party packages

Kutt integrates with a variety of languages and frameworks. See [upstream docs](https://github.com/thedevs-network/kutt#3rd-party-packages)
for more information


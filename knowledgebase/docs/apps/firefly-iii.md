# <img src="/img/firefly-iii-logo.png" width="25px"> Firefly III App

## Admin

Cloudron user can login to Firefly III using their username and password. This app has no 
separate 'admin' account. The first user to login is made an admin (site owner). Site owners
can edit and remove other users.

The Firefly III UI does not have a way to grant site owner permissions to another user.

## Sharing accounts

Currently, sharing account between users is not implemented. See the upstream bugtracker for
more information - [#372](https://github.com/firefly-iii/firefly-iii/issues/372)
and [#2531](https://github.com/firefly-iii/firefly-iii/issues/2531).


# <img src="/img/vault-logo.png" width="25px"> Vault App

## Setup

On first visit, you can generate the keys for the vault by specifying the number
of shares and the threshold. The idea is that a master key is generated and split
into the number of shares. To unlock the vault, you must provide atleast threshold count
of the parts. See [this blog](https://shadow-soft.com/vault-auto-unseal/) for a full
explanation on how Vault uses Shamir’s secret sharing algorithm.

## Unsealing

Vault stores all the secrets in memory. It uses the mlock capability of the kernel to
prevent swapping. When the app restarts or the server restarts, it must be unsealed using
the keys that were generated during setup. This is by Vault design.

## LDAP

After the Vault is unsealed, you can run the following command to enable Cloudron authentication.

```
    /app/pkg/enable-ldap.sh <root-token>
```

The root token is part of the keys generated during setup.


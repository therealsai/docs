# <img src="/img/bookstack-logo.png" width="25px"> BookStack App

## Admin

When using Cloudron user management, BookStack's built-in admin user is disabled.
See the [BookStack docs](https://www.bookstackapp.com/docs/admin/ldap-auth/) for
more information. In addition, the app is pre-setup to give admin status to all users.
You can change this by going to `Settings` -> `Registration` and adjusting the value of
`Default user role after registration`. This way, the first user to login will be an admin
and the roles of rest of the users can be managed inside BookStack.

## Customization

Use the [Web terminal](/apps#web-terminal)
to edit custom configuration under `/app/data/env`.

See [BookStack customization docs](https://www.bookstackapp.com/docs/admin/visual-customisation/)
for more information.

## External registration

Bookstack does not allow external users to register when Cloudron user management (LDAP)
is enabled. If you require external registration, install Bookstack with Cloudron user
management disabled.

See the [Bookstack docs](https://www.bookstackapp.com/docs/admin/third-party-auth/) to
enable 3rd party auth like Google, GitHub, Twitter, Facebook & others.


# <img src="/img/calibre-web-logo.png" width="25px"> Calibre Web App

## Importing existing calibre database

To import an existing Calibre database, do the following:

* Stop the app
* Copy the database into `/app/data/library` using the File Manager
* Start the app


# <img src="/img/directus-logo.png" width="25px"> Directus App

## CLI

CLI commands can be executed by opening a [Web terminal](/apps#web-terminal):

```
# cd /app/code
# sudo -u www-data -- /app/code/bin/directus
```


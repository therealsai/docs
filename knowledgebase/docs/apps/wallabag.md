# <img src="/img/wallabag-logo.png" width="25px"> Wallabag App

## Using the Browser Extensions

* [Firefox](https://addons.mozilla.org/fr/firefox/addon/wallabagger/)
* [Chrome](https://chrome.google.com/webstore/detail/wallabagger/gbmgphmejlcoihgedabhgjdkcahacjlj)

Follow the instructions in the [wallabag website](https://wallabag.org/en/news/wallabagger-howto)
to configure the extension to use the Cloudron app.

## Admin

To make an existing user an admin, first make note of the user's id (100 in the example below).
Then, run the following PostgreSQL command using the [Web Terminal](/apps#web-terminal):

```
UPDATE wallabag_user SET roles = 'a:2:{i:0;s:9:"ROLE_USER";i:1;s:16:"ROLE_SUPER_ADMIN";}' where id = 100;
```

